# CloudSwept Website

## Prerequisites

- [Node.js](http://nodejs.org/) >= v4 must be installed.
- [Yarn](https://yarnpkg.com/en/docs/install) >= 1.9.4 must be installed.
- [Parcel.js](https://www.npmjs.com/package/parcel-bundler) >= 1.7.1 must be installed.

## Installation

- Running `yarn install` in the app's root directory will install everything you need for development.

## Development Server

- `yarn start` will run the app's development server at [http://localhost:3000](http://localhost:3000), automatically reloading the page on every change.

## Building

- `yarn build` creates a production build by default.
- `yarn clean` will delete built resources.

## Deployment

- `yarn deploy` will deploy the website to netlify
