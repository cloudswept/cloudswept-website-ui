import './index.scss';

import jQuery from './jquery';
import 'jquery-parallax.js';
import InView from 'in-view';
import ParallaxGrid from './images/parallax-grid.png';
import ParallaxClouds from './images/parallax-clouds.png';

if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
  // Use the window load event to keep the page load performant
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('./sw.js');
  });
}

jQuery(($) => {
  // The web page is ready, we can do things now

  // Smooth scroll appears to be causing issues
  // smoothScroll();
  animationTrigger();
  parallax($);

});

function parallax($) {
  const os = getMobileOperatingSystem();

  // Parallax does not work on these platforms
  if (os === 'Android' || os === 'iOS') {
    return;
  }

  $('.parallax-grid').parallax({
    imageSrc: ParallaxGrid,
    bleed: 30,
    speed: 0.1
  });

  $('.parallax-clouds').parallax({
    imageSrc: ParallaxClouds,
    bleed: 150,
    speed: 0.1
  });
}

function animationTrigger() {
  if (!(document.body.classList.add instanceof Function)) {
    return;
  }

  const requirementsNotMet = [
    Array.from instanceof Function,
    document.querySelectorAll instanceof Function
  ]
    .filter(result => !result);

  // Something we want doesn't exist, don't carry on
  if (requirementsNotMet.length) {
    return;
  }

  const triggerInView = (element) => {
    const target = element.getAttribute('data-animation-target');
    Array.from(document.querySelectorAll(target))
      .forEach(targetElement => {
        targetElement.classList.add('animation-trigger-in-view');
      });
  };

  const selector = '.animation-trigger[data-animation-target]';

  Array.from(document.querySelectorAll(selector))
    .forEach(element => {
      if (!InView.is(element)) {
        return;
      }
      triggerInView(element);
    });

  InView(selector)
    .on('enter', triggerInView);
}

function smoothScroll() {
  // If filter doesn't exist, then we already know its old
  if (!(Array.prototype.filter instanceof Function)) {
    return;
  }

  const requirementsNotMet = [
    Array.from instanceof Function,
    document.querySelectorAll instanceof Function,
    document.body.scrollIntoView instanceof Function
  ]
    .filter(result => !result);

  // Something we want doesn't exist, don't carry on
  if (requirementsNotMet.length) {
    return;
  }

  // For each `a` tag with a `href` attribute starting with `#`
  Array.from(document.querySelectorAll('[href^="#"]'))
    .forEach(element => {

      // Add an event listener so we can do some smooth scrolling
      element.addEventListener('click', event => {

        // Get the original href
        const href = element.getAttribute('href');

        // Get the ID associated, we're going to find an element associated with it
        const id = href.replace(/^#/, '');
        const targetElement = document.getElementById(id);

        // If the target element doesn't actually exist, let the browser handle the scrolling
        if (!targetElement) {
          return;
        }

        // If it does exist, stop the default behaviour of the browser, as we are going to smooth scroll to it
        event.preventDefault();
        targetElement.scrollIntoView({
          behavior: 'smooth',
          // start means align the start of the element, with the start of the viewport
          block: 'start',
          inline: 'start'
        });

      })

    });
}

function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return "Windows Phone";
  }

  if (/android/i.test(userAgent)) {
    return "Android";
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }

  return "unknown";
}